# README #

Welcome to the parent repo for Walnut

### Here is a list of key folders ###

* Curious
* SQL Scripts
* NLU
* Project-Related Intellectual Property

### Curious  ###

* Main code repo, All the web/bot code lies here

### SQL Scripts ###

* Lates SQL dump from the data base

### NLU ###

* Contains the NLU extract from API.ai this is done to prevent loss of data and to eventually move to a custom location

###Things that could go wrong###
- After installation login to app and refresh the bot behavior . Else the bot does not know how to respond to event
- Check the end points are working
- Check the auth code is working, this should be ok in prod as the codes are automatically retrieved

###Project-Related Intellectual Property###
Access to this code repository is governed by the following clauses. Please read carefully before using or contributing any ip/code into the repository.

“Project IP” means:

(a) contributions and inventions, discoveries, creations, developments, improvements, works of authorship and ideas (whether or not protectable under patent, copyright, or other legal theory) of any kind that are conceived, created, developed or reduced to practice by any Founder, alone or with others, while such Founder is a member of, or provides services to, the Company, regardless of whether they are conceived or made during regular working hours or at the Company’s place of work, that are directly or indirectly related to the Project, result from tasks assigned to a Founder by the Company, or are conceived or made with the use of the Company’s resources, facilities or materials; and (b) any and all patents, patent applications, copyrights, trade secrets, trademarks (whether or not registered), domain names and other intellectual property rights, worldwide, with respect to any of the foregoing.

The term “Project IP” does not include any inventions developed by a Founder entirely on such Founder’s own time, without using any Company equipment, supplies, facilities or trade secret information, unless the invention related to the Project at the time of the invention’s conception or reduction to practice.

Each Founder hereby irrevocably assigns to the Company all right, title, and interest in and to all Project IP owned by such Founder. Each Founder agrees (i) to assist the Company from time to time with signing and filing any written documents of assignment that are necessary or expedient to evidence such Founder’s irrevocable assignment of Project IP to the Company; and (ii) to assist the Company in applying for, maintaining, and filing any renewals with respect to Project IP anywhere in the world, in each case at the Company’s expense.

Confidentiality
The Founders agree to keep all non-public information with respect to Project IP confidential and not to disclose it to any other party, except (i) to attorneys and advisors who need to know in connection with performing their duties, (ii) to potential business development partners and/or investors approved by the Company in writing, and who are bound by a confidentiality agreement in writing, and (iii) in response to an inquiry from a legal or regulatory authority.
